import React from "react";
const ChatHeaderCounter = ({ count, countName, children, className }) => {
  return (
    <p className={`m-0 mr-3 ${className ? className : ""}`} title={countName}>
      <b className="mr-1">{count}</b>
      {children}
    </p>
  );
};

export default React.memo(ChatHeaderCounter);
