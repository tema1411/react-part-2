import React from "react";
import ChatHeaderCounter from "./ChatHeaderCounter";

const ChatHeader = ({ participantsCount, messagesCount, lastMessageTime }) => (
  <section className="flex justify-between shadow-lg bg-white p-2 rounded">
    <div className="flex">
      <h1 className="mr-3 font-extrabold">My chat</h1>
      <ChatHeaderCounter
        count={participantsCount}
        countName="participants"
        className="text-red-500"
      >
        <i className="fas fa-users" />
      </ChatHeaderCounter>
      <ChatHeaderCounter
        count={messagesCount}
        countName="messages"
        className="text-blue-400"
      >
        <i className="fas fa-comments" />
      </ChatHeaderCounter>
    </div>
    <p>
      last message at <b>{lastMessageTime}</b>
    </p>
  </section>
);

export default React.memo(ChatHeader);
