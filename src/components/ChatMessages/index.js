import React, { useEffect } from "react";
import MessageList from "./MessageList";
import { scrollElementToBottom } from "../../helpers";

const ChatMessages = React.forwardRef(
  ({ messages, currentUser, controlLike, removeMessage, editMessage }, ref) => {
    const agoDates = [...messages.keys()];

    useEffect(() => {
      scrollElementToBottom(ref.current);
    }, []);

    return (
      <section
        className="flex flex-col list-none p-2 bg-white rounded flex-grow mt-4 mb-4 shadow-lg overflow-y-auto"
        ref={ref}
      >
        {agoDates.map((dateAgo) => (
          <MessageList
            controlLike={controlLike}
            dateAgo={dateAgo}
            messages={messages.get(dateAgo)}
            key={dateAgo}
            removeMessage={removeMessage}
            editMessage={editMessage}
            currentUser={currentUser}
          />
        ))}
      </section>
    );
  }
);

export default ChatMessages;
