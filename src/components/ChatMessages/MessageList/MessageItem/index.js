import React from "react";
import * as dayjs from "dayjs";

const MessageItem = ({
  messages,
  currentUser,
  controlLike,
  removeMessage,
  editMessage,
}) => {
  const { text, createdAt, user, userId, avatar, liked, id } = messages;
  const isCurrentUser = currentUser?.userId === userId;
  const bgColor = isCurrentUser ? "bg-blue-400" : "bg-blue-700";

  return (
    /*prettier-ignore*/
    <li
          className={`rounded  inline-flex align-top p-2 mb-2 ${bgColor} ${isCurrentUser && "self-end"}`}
          style={{maxWidth: "85%", width: 'fit-content'}}
      >
          {!isCurrentUser &&
              <img
                  className="object-cover object-center w-10 h-10 mr-2"
                  style={{borderRadius: '50%'}}
                  src={avatar}
                  alt={user}
              />
          }
          <div className='w-full'>
              <div className="flex justify-between items-baseline text-gray-100 text-sm">
                  <h5 className="mr-2 opacity-50 ">
                      {isCurrentUser
                          ? (
                              <button
                                  className="hover:underline focus:outline-none"
                                  onClick={() => editMessage(id, text)}
                              >
                                  Edit
                              </button>
                          )
                          : user
                      }
                  </h5>
                  <div className="flex items-center">
                      <small className="mr-2 opacity-50 ">{dayjs(createdAt).format("DD.MM.YY HH:mm")}</small>
                      {isCurrentUser
                          ?
                          <button type="button"
                                  className={`like outline-none focus:outline-none opacity-50 hover:opacity-75 ${liked && 'opacity-100'}`}
                                  onClick={() => removeMessage(id)}
                          >
                              <i className="fas fa-trash-alt" />
                          </button>
                          :
                          <button
                              type="button"
                              className={`like outline-none focus:outline-none opacity-50 hover:opacity-75 ${liked && 'opacity-100'}`}
                              onClick={() => controlLike(id)}
                          >
                              <i className="fas fa-heart" />
                          </button>
                      }
                  </div>
              </div>
              <p className="text-white" style={{wordBreak: 'break-word'}}>{text}</p>
          </div>
      </li>
  );
};

export default React.memo(MessageItem);
