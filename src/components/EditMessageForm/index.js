import MessageForm from "../MessageForm";
import React, { useState } from "react";

const EditMessageForm = ({ submitHandler, messageData, cancelEdit }) => {
  const { text, id } = messageData;
  const [message, setMessage] = useState(text);

  return (
    /*prettier-ignore*/
    <div className='fixed top-0 left-0 w-full h-full flex items-center justify-center'>
          <div className="absolute top-0 left-0 w-full h-full bg-gray-200 opacity-50" onClick={cancelEdit} />
          <div className="shadow-lg bg-white relative z-10 p-5 rounded" style={{width: '40vw', minWidth: 300, maxWidth: 600, height: 250,}}>
              <button
                  className="text-xl hover:text-blue-400 focus:text-blue-400 absolute right-0 top-0 mt-0 mr-1 outline-none focus:outline-none"
                  onClick={cancelEdit}>
                  <i className="fas fa-times-circle"/>
              </button>
              <MessageForm
                  isColumn={true}
                  submitHandler={() => submitHandler(id, message)}
                  onChange={(e) => setMessage(e.target.value)}
                  value={message}
              />
          </div>
      </div>
  );
};

export default EditMessageForm;
