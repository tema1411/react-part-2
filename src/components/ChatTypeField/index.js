import React, { useState } from "react";
import MessageForm from "../MessageForm";

const ChatTypeField = ({ addNewMessage, updateMessage, getLastMessage }) => {
  const [message, setMessage] = useState("");
  const [isFocus, setIsFocus] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const [idChangMessage, setIdChangMessage] = useState(false);

  const sendNewMessage = () => {
    addNewMessage(message);
    setMessage("");
  };

  const updateOldMessage = () => {
    updateMessage(idChangMessage, message);
    setMessage("");
    setIsEdit(false);
  };

  const handlerChangeLastMessage = (e) => {
    if (isFocus && !message.length && e.key === "ArrowUp") {
      setIsEdit(true);
      const { text, id } = getLastMessage();
      setMessage(text);
      setIdChangMessage(id);
    }
  };

  return (
    <MessageForm
      submitHandler={isEdit ? updateOldMessage : sendNewMessage}
      onChange={(e) => setMessage(e.target.value)}
      value={message}
      onFocus={() => setIsFocus(true)}
      onBlur={() => setIsFocus(false)}
      onKeyDown={handlerChangeLastMessage}
    />
  );
};

export default React.memo(ChatTypeField);
