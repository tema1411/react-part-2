import React from "react";
import loader from  './Eclipse-1s-200px.svg'

const Loader = (props) => (
    <div className="flex w-full h-full" {...props}>
        <img src={loader} alt="Loading..." {...props} width="90" height="90" className="m-auto"/>
    </div>
)



export default Loader;
