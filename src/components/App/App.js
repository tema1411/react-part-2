import React, { useEffect } from "react";
import "./App.css";
import AppHeader from "../AppHeader";
import AppFooter from "../AppFooter";
import Chat from "../Chat";
import { getMessage } from "../../api/message";
import { getRandomInt } from "../../helpers";
import { useDispatch } from "react-redux";
import { setMessages } from "../../store/message";
import { setCurrentUser } from "../../store/auth";

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    (async function uploadMessages() {
      const messagesData = await getMessage();
      dispatch(setMessages(messagesData));

      const { id, editedAt, createdAt, text, ...userData } = messagesData[
        getRandomInt(0, messagesData.length)
      ];
      dispatch(setCurrentUser(userData));
    })();
  }, []);

  return (
    <section className="flex flex-col justify-between h-screen bg-gray-200 ">
      <AppHeader />
      <Chat />
      <AppFooter />
    </section>
  );
}

export default App;
