import React, { useRef } from "react";
import ChatHeader from "../ChatHeader";
import Loader from "../Loader";
import ChatTypeField from "../ChatTypeField";
import ChatMessages from "../ChatMessages";
import {
  getGroupMessagesByDate,
  getLastMessageByUser,
  getParticipantsLength,
  getLastMessageTime,
} from "../../helpers/messageHelpers";
import { scrollElementToBottom } from "../../helpers";
import { useSelector, useDispatch } from "react-redux";
import {
  removeMessage,
  changeMessageLikeStatus,
  addNewMessage,
  updateMessageText,
} from "../../store/message";
import {
  finishMessageChange,
  startMessageChange,
} from "../../store/changeMessagePopup";
import EditMessageForm from "../EditMessageForm";

const HEADER_HEIGHT = "86px";
const FOOTER_HEIGHT = "40px";

const Chat = () => {
  const messages = useSelector((store) => store?.messages);
  const currentUser = useSelector((store) => store?.currentUser);
  const changingMessage = useSelector((store) => store?.changeMessage);

  const dispatch = useDispatch();

  const messageContainer = useRef(null);

  const saveNewMessage = (message) => {
    dispatch(addNewMessage({ message, currentUser }));
    setTimeout(() => scrollElementToBottom(messageContainer.current), 0);
  };

  const updateMessage = (id, text) => {
    dispatch(updateMessageText({ id, text }));
    dispatch(finishMessageChange());
  };

  if (messages) {
    return (
      <main
        className="flex flex-col justify-between flex-grow m-auto w-full h-full max-w-screen-md p-5 rounded"
        style={{ height: `calc(100% - ${HEADER_HEIGHT} - ${FOOTER_HEIGHT})` }}
      >
        <ChatHeader
          participantsCount={getParticipantsLength(messages)}
          messagesCount={messages?.length ?? 0}
          lastMessageTime={getLastMessageTime(messages)}
        />
        <ChatMessages
          messages={getGroupMessagesByDate([...messages])}
          removeMessage={(messageId) => dispatch(removeMessage(messageId))}
          controlLike={(messageId) =>
            dispatch(changeMessageLikeStatus(messageId))
          }
          editMessage={(id, text) => dispatch(startMessageChange({ id, text }))}
          currentUser={currentUser}
          ref={messageContainer}
        />
        <ChatTypeField
          addNewMessage={saveNewMessage}
          getLastMessage={() =>
            getLastMessageByUser([...messages], currentUser.userId)
          }
          updateMessage={(id, text) =>
            dispatch(updateMessageText({ id, text }))
          }
        />

        {changingMessage && (
          <EditMessageForm
            cancelEdit={() => dispatch(finishMessageChange())}
            messageData={changingMessage}
            submitHandler={updateMessage}
          />
        )}
      </main>
    );
  }

  return <Loader />;
};

export default React.memo(Chat);
