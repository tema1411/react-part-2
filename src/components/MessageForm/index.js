import React from "react";

const MessageForm = ({ submitHandler, isColumn, ...props }) => {
  const submit = (e) => {
    e.preventDefault();
    submitHandler();
  };

  return (
    <form
      className={`w-full bg-white shadow-lg flex p-2 w-full ${
        isColumn ? "flex-col h-full" : ""
      }`}
      onSubmit={submit}
    >
      <textarea
        required
        className={`border w-full mr-2 rounded resize-none p-2 flex-grow ${
          isColumn ? "mb-2" : ""
        }`}
        placeholder="Type message"
        {...props}
      />
      <button className="bg-blue-500 p-2 rounded text-white font-bold hover:bg-blue-500 focus: bg-blue-300 flex-shrink-0">
        Send
      </button>
    </form>
  );
};

export default React.memo(MessageForm);
