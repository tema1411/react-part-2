export const getRandomInt = (min, max) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
};

export const scrollElementToBottom = (el) => {
  el.scrollTop = el.scrollHeight;
};
