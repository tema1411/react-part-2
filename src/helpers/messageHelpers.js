import * as dayjs from "dayjs";

const isToday = require("dayjs/plugin/isToday");
const relativeTime = require("dayjs/plugin/relativeTime");
const isYesterday = require("dayjs/plugin/isYesterday");
dayjs.extend(isYesterday);
dayjs.extend(relativeTime);
dayjs.extend(isToday);

export const getParticipantsLength = (messages) => {
  const participants = new Set(messages.map(({ userId }) => userId));
  return participants.size;
};

export const getLastMessageTime = (messages) => {
  return dayjs(messages[messages.length - 1].createdAt).format("HH:mm");
};

export const getLastMessageByUser = (messages, userId) =>
  getSortedMessagesByTime(messages)
    .reverse()
    .find((message) => message.userId === userId);

const getSortedMessagesByTime = (messages) => {
  return messages.sort((messageA, messageB) => {
    if (dayjs(messageA.createdAt).isBefore(dayjs(messageB.createdAt))) {
      return -1;
    }
    if (dayjs(messageA.createdAt).isAfter(dayjs(messageB.createdAt))) {
      return 1;
    }
    return 0;
  });
};

export const getTimeAgoForGroupMessages = (date) => {
  const dayjsDate = dayjs(date);
  let timeAho;

  if (dayjsDate.isYesterday()) {
    timeAho = "Yesterday";
  } else if (dayjsDate.isToday()) {
    timeAho = "Today";
  } else {
    timeAho = dayjsDate.fromNow();
  }

  return timeAho;
};

export const getGroupMessagesByDate = (messages) => {
  const messagesGroupList = new Map();
  const sortedMessageByTime = getSortedMessagesByTime(messages);

  sortedMessageByTime.forEach((message) => {
    let timeMessageCreate = getTimeAgoForGroupMessages(message.createdAt);

    if (messagesGroupList.has(timeMessageCreate)) {
      messagesGroupList.set(timeMessageCreate, [
        ...messagesGroupList.get(timeMessageCreate),
        message,
      ]);
      return;
    }

    messagesGroupList.set(timeMessageCreate, [message]);
  });

  return messagesGroupList;
};
