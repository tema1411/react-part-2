import {createAction, createReducer} from "@reduxjs/toolkit";

export const setCurrentUser = createAction("SET_CURRENT_USER");

const authReducer = createReducer(null, {
    [setCurrentUser]: (state, {payload}) => payload,
})

export default authReducer;
