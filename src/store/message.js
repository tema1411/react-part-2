import { createAction, createReducer } from "@reduxjs/toolkit";
import nextId from "react-id-generator";

export const setMessages = createAction("SET_MESSAGES");
export const removeMessage = createAction("REMOVE_MESSAGE");
export const changeMessageLikeStatus = createAction("CHANGE_LIKE_STATUS");
export const updateMessageText = createAction("UPDATE_MESSAGE_TEXT");
export const addNewMessage = createAction(
  "ADD_NEW_MESSAGE",
  ({ message: text, currentUser }) => ({
    payload: {
      id: nextId(),
      text,
      user: currentUser.user,
      userId: currentUser.userId,
      editedAt: "",
      createdAt: new Date().toISOString(),
    },
  })
);

const messagesReducer = createReducer(null, {
  [setMessages]: (state, { payload }) => payload,
  /*prettier-ignore*/
  [removeMessage]: (state, { payload }) => state.filter((message) => payload !== message.id),
  [changeMessageLikeStatus]: (state, { payload }) =>
    state.map((message) =>
      payload === message.id ? { ...message, liked: !message?.liked } : message
    ),
  [addNewMessage]: (state, { payload }) => [...state, payload],
  [updateMessageText]: (state, { payload }) =>
    state.map((message) =>
      payload.id === message.id ? { ...message, text: payload.text } : message
    ),
});

export default messagesReducer;
