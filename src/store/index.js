import { configureStore } from "@reduxjs/toolkit";
import authReducer from "./auth";
import messagesReducer from "./message";
import changeMessageWindowReducer from "./changeMessagePopup";

const INITIAL_STATE = {};

const REDUCERS = {
  messages: messagesReducer,
  currentUser: authReducer,
  changeMessage: changeMessageWindowReducer,
};

const store = configureStore({
  reducer: REDUCERS,
  devTools: true,
  preloadedState: INITIAL_STATE,
});

export default store;
