import { createAction, createReducer } from "@reduxjs/toolkit";

export const startMessageChange = createAction("START_MESSAGE_CHANGE");
export const finishMessageChange = createAction("FINISH_MESSAGE_CHANGE");

const changeMessagePopupReducer = createReducer(null, {
  [startMessageChange]: (state, { payload }) => payload,
  [finishMessageChange]: () => null,
});

export default changeMessagePopupReducer;
